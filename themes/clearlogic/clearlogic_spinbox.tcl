# clearlogic.tcl

namespace eval ttk::theme::clearlogic {

    package provide ttk::theme::clearlogic 0.1

    proc LoadImages {imgdir {patterns {*.gif}}} {
        foreach pattern $patterns {
            foreach file [glob -directory $imgdir $pattern] {
                set img [file tail [file rootname $file]]
                if {![info exists images($img)]} {
                    set images($img) [image create photo -file $file]
                }
            }
        }
        return [array get images]
    }

    variable I
    array set I [LoadImages \
                     [file join [file dirname [info script]] clearlogic] *.gif]
    
    variable colors

    array set colors {
        -frame          "#efebe7"
        -lighter        "#f5f3f0"
        -dark           "#cfcdc8"
        -darker         "#9e9a9e"
        -darkest        "#d4cfca"
        -selectbg       "#657a9e"
        -selectbg__     "#7c99ad"
        -selectfg       "#ffffff"
        -disabledfg     "#b5b3ac"
        -entryfocus     "#6f9dc6"
        -tabbg          "#e5e3e1"
        -tabbg__        "#c9c1bc"
        -tabborder      "#b5aca7"
        -troughcolor    "#d7cbbe"
        -troughborder   "#ae9e8e"
        -checklight     "#f5f3f0"
        -window		    "#ffffff"
    }


    ttk::style theme create clearlogic -parent clam -settings {

        ttk::style configure . \
            -borderwidth        1 \
            -background         $colors(-frame) \
            -foreground         black \
            -bordercolor        $colors(-darkest) \
            -darkcolor          $colors(-dark) \
            -lightcolor         $colors(-lighter) \
            -troughcolor        $colors(-troughcolor) \
            -selectforeground   $colors(-selectfg) \
            -selectbackground   $colors(-selectbg) \
            -font               TkDefaultFont \
            ;

        ttk::style map . \
            -background [list disabled $colors(-frame) \
                              active $colors(-lighter)] \
            -foreground [list disabled $colors(-disabledfg)] \
            -selectbackground [list !focus $colors(-darker)] \
            -selectforeground [list !focus white] \
            ;


        # ttk::style configure Frame.border -relief groove

        ## Treeview.
        #
        ttk::style element create Treeheading.cell image \
            [list $I(tree-n) \
                selected $I(tree-p) \
                disabled $I(tree-d) \
                pressed $I(tree-p) \
                active $I(tree-h) \
                ] \
            -border 4 -sticky ew

        ttk::style configure Row -background "#efefef"

        ttk::style element create Treeitem.indicator from alt

        ttk::style configure Treeview -background $colors(-window)
        ttk::style map Treeview \
                -background [list selected $colors(-selectbg)] \
                -foreground [list selected $colors(-selectfg)]

                
        ## Buttons.
        #
        ttk::style configure TButton -padding {3 0} -anchor center
        ttk::style layout TButton {
            Button.button -children {
                Button.focus -children {
                    Button.padding -children {
                        Button.label
                    }
                }
            }
        }
        ttk::style element create Button.button image \
            [list $I(button-n) \
                 pressed $I(button-p) \
                 {selected active} $I(button-pa) \
                 selected $I(button-p) \
                 active $I(button-a) \
                 disabled $I(button-d) \
                ] \
            -border {3 3} -padding 3 -sticky nsew
            
            
        ttk::style configure IconButton -padding {0 0}
        ttk::style layout IconButton {
            IconButton.button -children {
                IconButton.focus -children {
                    IconButton.padding -children {
                        IconButton.label -sticky nsew
                    }
                }
            }
        }
        ttk::style element create IconButton.button image \
            [list $I(iconbutton-n) \
                pressed $I(buttonflat-p) \
                {selected active} $I(buttonflat-pa) \
                selected $I(buttonflat-p) \
                active $I(buttonflat-a) \
                disabled $I(buttonflat-d)] \
            -border {3 3} -padding 3 -sticky nsew
            
        ttk::style configure LowButton -padding {3 0} -anchor center
        ttk::style layout LowButton {
            LowButton.button -children {
                LowButton.focus -children {
                    LowButton.padding -children {
                        LowButton.label -sticky nsew
                    }
                }
            }
        }
        ttk::style element create LowButton.button image \
            [list $I(lowbutton-n) \
                pressed $I(lowbutton-p) \
                {selected active} $I(lowbutton-pa) \
                selected $I(lowbutton-p) \
                active $I(lowbutton-a) \
                disabled $I(lowbutton-d)] \
            -border {3 3} -padding 3 -sticky nsew        
        #ttk::style element create LowButton.button image \
            [list $I(button-n) \
                pressed $I(button-n) \
                {selected active} $I(button-pa) \
                selected $I(button-p) \
                active $I(button-a) \
                disabled $I(button-d)] \
            -border {3 3} -padding 3 -sticky nsew
        #ttk::style element create LowButton.button image \
            [list $I(lowbutton-n) \
                pressed $I(lowbutton-p) \
                {selected active} $I(lowbutton-pa) \
                selected $I(lowbutton-p) \
                active $I(lowbutton-a) \
                disabled $I(lowbutton-d)] \
            -border {3 3} -padding 3 -sticky nsew
            
        # ttk::style element create Button.button image \
            [list $I(button-n) \
                 pressed $I(button-p) \
                 {selected active} $I(button-pa) \
                 selected $I(button-p) \
                 active $I(button-a) \
                 disabled $I(button-d) \
                ] \
            -border {3 3} -padding 3

        # ttk::style configure Icon.TButton -padding {3 0}
        
        # ttk::style element create button image \
            [list $I(button-n) \
                 pressed $I(button-p) \
                 {selected active} $I(button-pa) \
                 selected $I(button-p) \
                 active $I(button-a) \
                 disabled $I(button-d) \
                ] \
            -border {3 3} -padding 3
                # -border 4 -sticky ew
            # ttk::style configure Icon.TButton {*}[ttk::style configure TButton] \
            # -background red
            # -background "#dedbd6"
        

        ## Checkbuttons.
        #
        ttk::style element create Checkbutton.indicator image \
            [list $I(check-nu) \
                {disabled selected} $I(check-dc) \
                disabled $I(check-du) \
                {pressed selected} $I(check-pc) \
                pressed $I(check-pu) \
                {active selected} $I(check-ac) \
                active $I(check-au) \
                selected $I(check-nc) ] \
            -width 24 -sticky w

        ttk::style map TCheckbutton -background [list active $colors(-checklight)]
        ttk::style configure TCheckbutton -padding 1


        ## Radiobuttons.
        #
        ttk::style element create Radiobutton.indicator image \
             [list $I(radio-nu) \
                  {disabled selected} $I(radio-dc) \
                  disabled $I(radio-du) \
                  {pressed selected} $I(radio-pc) \
                  pressed $I(radio-pu) \
                  {active selected} $I(radio-ac) \
                  active $I(radio-au) \
                  selected $I(radio-nc) ] \
            -width 24 -sticky w

        ttk::style map TRadiobutton -background [list active $colors(-checklight)]
        ttk::style configure TRadiobutton -padding 1


        ## Menubuttons.
        #
        #ttk::style configure TMenubutton -relief raised -padding {10 2}
        # 	ttk::style element create Menubutton.border image $I(toolbutton-n) \
        # 	    -map [list \
        #                       pressed $I(toolbutton-p) \
        #                       selected $I(toolbutton-p) \
        #                       active $I(toolbutton-a) \
        #                       disabled $I(toolbutton-n)] \
        # 	    -border {4 7 4 7} -sticky nsew
        if 0 {
            ttk::style element create Menubutton.border image \
                 [list $I(button-n) \
                      selected $I(button-p) \
                      disabled $I(button-d) \
                      active $I(button-a) \
                     ] \
                -border 4 -sticky ew
                
            ttk::style element create Menubutton.button image \
                [list $I(iconbutton-n) \
                    pressed $I(buttonflat-p) \
                    {selected active} $I(buttonflat-pa) \
                    selected $I(buttonflat-p) \
                    active $I(buttonflat-a) \
                    disabled $I(buttonflat-d)] \
                -border {3 3} -padding 3 -sticky nsew

        }
        ttk::style element create Menubutton.border image \
                [list $I(button-n) \
                    selected $I(button-p) \
                    disabled $I(button-d) \
                    active $I(button-a) ] \
                -border 4 -sticky ew
                
        ttk::style layout IconMenubutton {
            IconMenubutton.button -children {
                IconMenubutton.indicator -side right
                IconMenubutton.focus -children {
                    IconMenubutton.padding -children {
                        IconMenubutton.label -side left -expand true
                    }
                }
            }
        }
        ttk::style configure IconMenubutton
        ttk::style element create IconMenubutton.button image \
                [list $I(iconmenubutton-n) \
                    selected $I(iconmenubutton-p) \
                    active $I(iconmenubutton-a) \
                    disabled $I(iconmenubutton-d)] \
                -border {3 3 3 3} -padding {3 3 3} -sticky nsew
        ttk::style element create IconMenubutton.indicator image \
                [list $I(iconarrowdown-n) \
                    disabled $I(iconarrowdown-d)] \
                -sticky e -border {0 0 0 0}        
                
                

        ## Toolbar buttons.
        #
        ttk::style configure Toolbutton -padding -5 -relief flat
        ttk::style configure Toolbutton.label -padding 0 -relief flat

        ttk::style element create Toolbutton.border image \
            [list $I(blank) \
                 pressed $I(toolbutton-p) \
                 {selected active} $I(toolbutton-pa) \
                 selected $I(toolbutton-p) \
                 active $I(toolbutton-a) \
                 disabled $I(blank)] \
            -border 11 -sticky nsew


        ## Entry widgets.
        #
        ttk::style configure TEntry -padding 1 -insertwidth 1 \
            -fieldbackground white

        ttk::style map TEntry \
            -fieldbackground [list readonly $colors(-frame)] \
            -bordercolor     [list focus $colors(-selectbg)] \
            -lightcolor      [list focus $colors(-entryfocus)] \
            -darkcolor       [list focus $colors(-entryfocus)] \
            ;
            
        ttk::style layout FlatEntry {
            Entry.plain.field -children {
                Entry.background -children { 
                    Entry.padding -children {
                        Entry.textarea -sticky nswe
                    }
                    -sticky nswe
                }
                -sticky nswe
            }
            -border 0 -sticky nswe
        }

        ttk::style configure FlatEntry \
                -background $colors(-frame) \
                -foreground "black" \
                -fieldbackground $colors(-frame)
            
            
        ## Spinbox widgets.
        #
        ttk::style element create LowSpinbox.uparrow vsapi \
            SPIN 1 {disabled 4 pressed 3 active 2 {} 1} \
            -padding 1 -halfheight 1 \
            -syssize { SM_CXVSCROLL SM_CYVSCROLL }
        ttk::style element create LowSpinbox.downarrow vsapi \
            SPIN 2 {disabled 4 pressed 3 active 2 {} 1} \
            -padding 1 -halfheight 1 \
            -syssize { SM_CXVSCROLL SM_CYVSCROLL }

        # ttk::style map LowSpinbox \
            -selectbackground [list readonly $colors(-frame)] \
            ;
            
        ttk::style layout LowSpinbox {
            LowSpinbox.field  -side top  -sticky we -children {
                null  -side right  -sticky {}  -children {  
                    LowSpinbox.uparrow -side top -sticky e 
                    LowSpinbox.downarrow -side bottom -sticky e
                }
                Spinbox.padding  -sticky nswe  -children { 
                    Spinbox.textarea -sticky nswe 
                }
            }
        }
        
        
        
        # ttk::style configure TSpinbox -padding 1 -insertwidth 10 \
            -arrowsize 2
            

        #   ttk::style configure IconButton -padding {0 0}
        #   ttk::style layout IconButton {
        #       IconButton.button -children {
        #           IconButton.focus -children {
        #               IconButton.padding -children {
        #                   IconButton.label -sticky nsew
        #               }
        #           }
        #       }
        #   }
        #   ttk::style element create IconButton.button image \
        #       [list $I(iconbutton-n) \
        #           pressed $I(buttonflat-p) \
        #           {selected active} $I(buttonflat-pa) \
        #           selected $I(buttonflat-p) \
        #           active $I(buttonflat-a) \
        #           disabled $I(buttonflat-d)] \
        #       -border {3 3} -padding 3 -sticky nsew            
            
            

        ## Combobox.
        #
        ttk::style configure TCombobox -selectbackground

        ttk::style element create Combobox.downarrow image \
            [list $I(comboarrow-n) \
                 disabled $I(comboarrow-d) \
                 pressed $I(comboarrow-p) \
                 active $I(comboarrow-a) \
                ] \
            -border 1 -sticky {}

        ttk::style element create Combobox.field image \
            [list $I(combo-n) \
                 {readonly disabled} $I(combo-rd) \
                 {readonly pressed} $I(combo-rp) \
                 {readonly focus} $I(combo-rf) \
                 readonly $I(combo-rn) \
                ] \
            -border 4 -sticky ew


        ## Notebooks.
        #
        # ttk::style element create tab image $I(tab-a) -border {2 2 2 0} \
        #     -map [list selected $I(tab-n)]
        ttk::style configure TNotebook -padding {0 1 0 0}
        ttk::style configure TNotebook.Tab -border {2 2 2 2}
            # ttk::style configure TNotebook.Tab -padding {6 2 6 2}
        ttk::style map TNotebook.Tab \
            -padding    [list selected {6 1 6 1} {} {2 0 2 0}] \
            -background [list selected $colors(-selectfg)   {} $colors(-frame)] \
            -lightcolor [list selected $colors(-lighter)    {} $colors(-dark)] \
            ;
        # -background [list selected $colors(-frame)   {} $colors(-tabbg)] \
        # -lightcolor [list selected $colors(-lighter) {} $colors(-dark)] \
        # -bordercolor [list selected $colors(-darkest) {} $colors(-tabborder)] \
        # -padding [list selected {6 4 6 2}] \
        # -padding {0 2 0 0} -border {4 10 4 10}
        # ttk::style element create Notebook.client image $I(notebook-c) -border 4
        # ttk::style element create Notebook.tab image [list $I(notebook-tn) \
        #         selected	$I(notebook-ts) \
        #         active	$I(notebook-ta) \
        #     ] -padding {0 2 0 0} -border {4 10 4 10}

        ## Labelframes.
        #
        ttk::style configure TLabelframe -borderwidth 2 -relief groove


        ## Scrollbars.
        #
        ttk::style layout Vertical.TScrollbar {
            Scrollbar.trough -sticky ns -children {
                Scrollbar.uparrow -side top
                Scrollbar.downarrow -side bottom
                Vertical.Scrollbar.thumb -side top -expand true -sticky ns
            }
        }

        ttk::style layout Horizontal.TScrollbar {
            Scrollbar.trough -sticky we -children {
                Scrollbar.leftarrow -side left
                Scrollbar.rightarrow -side right
                Horizontal.Scrollbar.thumb -side left -expand true -sticky we
            }
        }

        ttk::style element create Horizontal.Scrollbar.thumb image \
            [list $I(sbthumb-hn) \
                 disabled $I(sbthumb-hd) \
                 pressed $I(sbthumb-ha) \
                 active $I(sbthumb-ha)] \
            -border 3

        ttk::style element create Vertical.Scrollbar.thumb image \
            [list $I(sbthumb-vn) \
                 disabled $I(sbthumb-vd) \
                 pressed $I(sbthumb-va) \
                 active $I(sbthumb-va)] \
            -border 3

        foreach dir {up down left right} {
            ttk::style element create ${dir}arrow image \
                [list $I(arrow${dir}-n) \
                     disabled $I(arrow${dir}-d) \
                     pressed $I(arrow${dir}-p) \
                     active $I(arrow${dir}-a)] \
                -border 1 -sticky {}
        }

        ttk::style configure TScrollbar -bordercolor $colors(-troughborder)


        ## Scales.
        #
        ttk::style element create Scale.slider image \
            [list $I(scale-hn) \
                 disabled $I(scale-hd) \
                 active $I(scale-ha) \
                ]

        ttk::style element create Scale.trough image $I(scaletrough-h) \
            -border 2 -sticky ew -padding 0

        ttk::style element create Vertical.Scale.slider image \
            [list $I(scale-vn) \
                 disabled $I(scale-vd) \
                 active $I(scale-va) \
                ]
        ttk::style element create Vertical.Scale.trough image $I(scaletrough-v) \
            -border 2 -sticky ns -padding 0

        ttk::style configure TScale -bordercolor $colors(-troughborder)


        ## Progressbar.
        #
        ttk::style element create Horizontal.Progressbar.pbar image $I(progress-h) \
            -border {2 2 1 1}
        ttk::style element create Vertical.Progressbar.pbar image $I(progress-v) \
            -border {2 2 1 1}

        ttk::style configure TProgressbar -bordercolor $colors(-troughborder)


        ## Statusbar parts.
        #
        ttk::style element create sizegrip image $I(sizegrip)


        ## Paned window parts.
        #
        # ttk::style element create hsash image $I(hseparator-n) -border {2 0} \
        #     -map [list {active !disabled} $I(hseparator-a)]
        # ttk::style element create vsash image $I(vseparator-n) -border {0 2} \
        #     -map [list {active !disabled} $I(vseparator-a)]

        ttk::style configure Sash -sashthickness 6 -gripcount 16


        ## Separator.
        #
        #ttk::style element create separator image $I(sep-h)
        #ttk::style element create hseparator image $I(sep-h)
        #ttk::style element create vseparator image $I(sep-v)

    }

}

