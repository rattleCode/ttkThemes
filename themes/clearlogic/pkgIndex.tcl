# Package index for tile demo pixmap themes.

if {[file isdirectory [file join $dir clearlogic]]} {
    if {![catch {package require Ttk}]} {
        package ifneeded ttk::theme::clearlogic 0.1 \
            [list source [file join $dir clearlogic.tcl]]
    } else {
        return
    }
}

