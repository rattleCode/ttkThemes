#!/bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"

#
# ttkThemes collects several ttk::themes
#

package provide ttkThemes 0.01

if {[catch {package require Ttk}]} {
    package require tile
}

namespace eval ttk_Themes {
        #
    variable packageHomeDir [file dirname [file normalize [info script]]]
        #
    if {[file isdirectory [file join $packageHomeDir themes]]} {
            #puts " --- ttk_Themes ---"
        lappend auto_path [file join $packageHomeDir themes]
            # This forces an update of the available packages list.
            # It's required for package names to find the themes in demos/themes/*.tcl
        eval [package unknown] Tcl [package provide Tcl]
            #
        if 1 {
            foreach name [ttk::themes] {
                puts "    ... $name"
            }
        }
    }
        #
}